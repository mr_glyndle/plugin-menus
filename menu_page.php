<?php
/*
Template Name: Menu page
Description: Add a menu 'a la restaurant food list'
*/
get_header();
while (have_posts()) : the_post();
	$item_count = 1;
	$GLOBALS['item_count'] = "1";

	include(plugin_dir_path( __FILE__ ) . 'full_menu.php');

	// check if the flexible content field has rows of data
    if( have_rows('menu_cont') ) {
		// loop through the rows of data
		while ( have_rows('menu_cont') ) : the_row();

			include(locate_template('partials/slice_loop.php'));

		endwhile;
	};
endwhile;

if (comments_open() || get_comments_number()) {
	echo '<div class="txt_blk comments">';
	comments_template();
	echo '</div>';
};

get_footer();
