<?php
/*
Plugin Name: 			Food Menus
Plugin URI: 			https://bitbucket.org/thebrandchap/plugin-menus/
Description: 			Creates a custom post type for displaying Food and Drink Menus including Schema data for the Google robots. Requires Sitebooster Theme and ACF.
Version:           		1.0.0
Author:            		Glyn Harrison
License:           		GNU General Public License v2
License URI:       		http://www.gnu.org/licenses/gpl-2.0.html
Bitbucket Plugin URI: https://bitbucket.org/thebrandchap/plugin-menus/
*/



// Add "Custom" template to page attirbute template section.
function wpse_288589_add_template_to_select( $post_templates, $wp_theme, $post, $post_type ) {

    // Add custom template named menu_page.php to select dropdown
    $post_templates['menu_page.php'] = __('Menu');

    return $post_templates;
}

add_filter( 'theme_page_templates', 'wpse_288589_add_template_to_select', 10, 4 );



/**
 * Check if current page has our custom template. Try to load
 * template from theme directory and if not exist load it
 * from root plugin directory.
 */
function wpse_288589_load_plugin_template( $template ) {

    if(  get_page_template_slug() === 'menu_page.php' ) {

        if ( $theme_file = locate_template( array( 'menu_page.php' ) ) ) {
            $template = $theme_file;
        } else {
            $template = plugin_dir_path( __FILE__ ) . 'menu_page.php';
        }
    }

    if($template == '') {
        throw new \Exception('No template found');
    }

    return $template;
}
add_filter( 'template_include', 'wpse_288589_load_plugin_template' );




/**
* Load custom fields
*/
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5787ace992627',
	'title' => 'MENU - Page',
	'fields' => array(
		array(
			'key' => 'field_59676536be78b',
			'label' => 'Header Settings',
			'name' => 'head_set',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_57694dc34b52a',
			),
			'display' => 'group',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 1,
		),
		array(
			'key' => 'field_5bd6ef9af6940',
			'label' => 'Menu Area',
			'name' => '',
			'type' => 'accordion',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'open' => 0,
			'multi_expand' => 0,
			'endpoint' => 0,
		),
		array(
			'key' => 'field_5787ace9ad071',
			'label' => 'Content',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 1,
		),
		array(
			'key' => 'field_5947ee51b2c25',
			'label' => 'Menu Name',
			'name' => 'sname',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5bd1e1fae2a9f',
			'label' => 'Served from',
			'name' => 'served_from',
			'type' => 'time_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'g:i a',
			'return_format' => 'H:i:s',
		),
		array(
			'key' => 'field_5bd1e237e2aa0',
			'label' => 'Served to',
			'name' => 'served_to',
			'type' => 'time_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'display_format' => 'g:i a',
			'return_format' => 'H:i:s',
		),
		array(
			'key' => 'field_5787ace9ad07b',
			'label' => 'Make the menu full width?',
			'name' => 'full_width',
			'type' => 'checkbox',
			'instructions' => 'If you\'re using a full width site, do you want the menu sections to fill the whole width of the screen?',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'yes' => 'Yes',
			),
			'allow_custom' => 0,
			'default_value' => array(
				0 => 'yes',
			),
			'layout' => 'vertical',
			'toggle' => 0,
			'return_format' => 'value',
			'save_custom' => 0,
		),
		array(
			'key' => 'field_5787ace9ad085',
			'label' => 'Intro Text?',
			'name' => 'intro_text',
			'type' => 'wysiwyg',
			'instructions' => 'Text to go before menu sections',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
			'delay' => 1,
		),
		array(
			'key' => 'field_5787ace9ad08f',
			'label' => 'Menu Section',
			'name' => 'menu_section',
			'type' => 'repeater',
			'instructions' => 'This is the top level section eg. \'Wines\'',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_5787acead7b0e',
			'min' => 0,
			'max' => 0,
			'layout' => 'block',
			'button_label' => 'Add Main Menu Section eg. \'Wines\'',
			'sub_fields' => array(
				array(
					'key' => 'field_5787acead7b0e',
					'label' => 'Main Section Header',
					'name' => 'section_header',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '100',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array(
					'key' => 'field_5bd1e2cbe2aa2',
					'label' => 'Section description',
					'name' => 'main_desc',
					'type' => 'textarea',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				),
				array(
					'key' => 'field_5787ae10a606a',
					'label' => 'Sub Section',
					'name' => 'menu_sub_section',
					'type' => 'repeater',
					'instructions' => 'This is the sub section eg. \'White Wines\'',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '100',
						'class' => '',
						'id' => '',
					),
					'collapsed' => 'field_5787ae7aa606b',
					'min' => 0,
					'max' => 0,
					'layout' => 'block',
					'button_label' => 'Add Sub Section eg. \'White Wines\'',
					'sub_fields' => array(
						array(
							'key' => 'field_5787ae7aa606b',
							'label' => 'Sub Section Header',
							'name' => 'sub_section_header',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => 60,
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'readonly' => 0,
							'disabled' => 0,
						),
						array(
							'key' => 'field_5787acead7b1c',
							'label' => 'Price header',
							'name' => 'price_header',
							'type' => 'text',
							'instructions' => '',
							'required' => '',
							'conditional_logic' => '',
							'wrapper' => array(
								'width' => 30,
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'readonly' => 0,
							'disabled' => 0,
						),
						array(
							'key' => 'field_5bd6f3ba46b49',
							'label' => 'Sub Section Description',
							'name' => 'sub_sec_desc',
							'type' => 'textarea',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '60',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'maxlength' => '',
							'rows' => '',
							'new_lines' => '',
						),
						array(
							'key' => 'field_5787acead7b26',
							'label' => 'Menu Section Items',
							'name' => 'menu_items',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '100',
								'class' => '',
								'id' => '',
							),
							'collapsed' => 'field_5787aceadc60b',
							'min' => 0,
							'max' => 0,
							'layout' => 'block',
							'button_label' => 'Add Menu Item',
							'sub_fields' => array(
								array(
									'key' => 'field_5787aceadc60b',
									'label' => 'Name',
									'name' => 'name',
									'type' => 'text',
									'instructions' => 'What\'s it called',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => 60,
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
									'readonly' => 0,
									'disabled' => 0,
								),
								array(
									'key' => 'field_5787aceadc619',
									'label' => 'Price',
									'name' => 'price',
									'type' => 'text',
									'instructions' => 'How much?',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => 20,
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
									'readonly' => 0,
									'disabled' => 0,
								),
								array(
									'key' => 'field_5787aceadc624',
									'label' => 'Symbol(s)',
									'name' => 'symbol',
									'type' => 'text',
									'instructions' => 'eg. \' v g \' for Vegetarian & Gluten free',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '20',
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'prepend' => '',
									'append' => '',
									'maxlength' => '',
								),
								array(
									'key' => 'field_5787aceadc62e',
									'label' => 'Description',
									'name' => 'description',
									'type' => 'textarea',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => 100,
										'class' => '',
										'id' => '',
									),
									'default_value' => '',
									'placeholder' => '',
									'maxlength' => '',
									'rows' => '',
									'new_lines' => 'br',
									'readonly' => 0,
									'disabled' => 0,
								),
								array(
									'key' => 'field_5787aceadc638',
									'label' => 'Add Options',
									'name' => 'options',
									'type' => 'repeater',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array(
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'collapsed' => 'field_5787aceae3e65',
									'min' => 0,
									'max' => 0,
									'layout' => 'block',
									'button_label' => 'Add Option(s)',
									'sub_fields' => array(
										array(
											'key' => 'field_5787aceae3e65',
											'label' => 'Option',
											'name' => 'option',
											'type' => 'text',
											'instructions' => '',
											'required' => 0,
											'conditional_logic' => 0,
											'wrapper' => array(
												'width' => '',
												'class' => '',
												'id' => '',
											),
											'default_value' => '',
											'placeholder' => '',
											'prepend' => '',
											'append' => '',
											'maxlength' => '',
										),
									),
								),
							),
						),
					),
				),
			),
		),
		array(
			'key' => 'field_5787ace9ad098',
			'label' => 'Notes',
			'name' => 'notes',
			'type' => 'textarea',
			'instructions' => '',
			'required' => '',
			'conditional_logic' => '',
			'wrapper' => array(
				'width' => 100,
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => '',
			'new_lines' => 'br',
			'readonly' => 0,
			'disabled' => 0,
		),
		array(
			'key' => 'field_5947e9ec11cd9',
			'label' => 'Overlay',
			'name' => 'overlay',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_590b2b9b33fb8',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
		),
		array(
			'key' => 'field_5947ea0511cda',
			'label' => 'Background',
			'name' => 'background',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_58fdf8606236d',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
		),
		array(
			'key' => 'field_5bd6efbcf6941',
			'label' => 'Menu area end',
			'name' => '',
			'type' => 'accordion',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'open' => 0,
			'multi_expand' => 0,
			'endpoint' => 1,
		),
		array(
			'key' => 'field_5976046e7d27d',
			'label' => 'Extra content',
			'name' => 'menu',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_5ab28d0b3cfd3',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 1,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_template',
				'operator' => '==',
				'value' => 'menu_page.php',
			),
		),
	),
	'menu_order' => 27,
	'position' => 'acf_after_title',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'field',
	'hide_on_screen' => array(
		0 => 'the_content',
	),
	'active' => true,
	'description' => '',
));

endif;
