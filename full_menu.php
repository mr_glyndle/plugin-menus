<?php
$slice_type = 'menu_layout';
include(locate_template('partials/overlay.php'));
include(locate_template('partials/spacing.php'));
$full_width = get_field('full_width');
if($full_width){
	$full_width = $full_width[0];
};
$intro_text = get_field('intro_text');

$container_class = 'menu full_menu';
if ($full_width == 'yes') { $container_class = $container_class.' full'; };


$GLOBALS['footer-schema'] .= '
	"@type": "FoodEstablishment",
	"name":"' . get_bloginfo('name') . '",
	"ID":"' . get_permalink() . '#FoodEstablishment"';
	if(get_the_post_thumbnail_url()) {
		$GLOBALS['footer-schema'] .= ', "image": "' . get_the_post_thumbnail_url(get_the_ID(),'golden_medium') . '"';
	}

	if(get_permalink()) {
		$GLOBALS['footer-schema'] .= ',"url": "' . get_permalink() . '"';
	};

	if(get_permalink()) {
		$GLOBALS['footer-schema'] .= ', "mainEntityOfPage": "' . get_permalink() . '"';
	};

	if (get_field('business_desc', 'option')) {
		$GLOBALS['footer-schema'] .= ',"description":"' . get_field('business_desc', 'option') . '"';
	}

	// loop through the rows Main Menu Sections
	$menu_section_count = get_field('menu_section');
	$menu_section_count = count($menu_section_count);
	$current_section = 1;

	if (have_rows('menu_section')) {

		$GLOBALS['footer-schema'] .= '
		,"hasMenu":{
			"@type":"Menu",
			"name":"' . get_the_title() . '",
			"description":"' . get_the_excerpt() . '",
			"ID":"' . get_permalink() . '#Menu"';
			if (get_field('served_to') && get_field('served_from')) {
				$served_to = 'T' . get_field('served_to');
				$served_from = 'T' . get_field('served_from');
				$GLOBALS['footer-schema'] .= ',"offers":{
					"@type":"Offer",
					"availabilityStarts":"' . $served_from . '",
					"availabilityEnds":"' . $served_to . '"
				}';
			}
			$GLOBALS['footer-schema'] .= ',"hasMenuSection":[';



		while (have_rows('menu_section')) : the_row();
			// loop through the rows Menu Sub Sections
			$menu_sub_section_count = get_sub_field('menu_sub_section');
			$menu_sub_section_count = count($menu_sub_section_count);
			$current_sub_section = 1;



			// loop through the rows Sub Menu Sections
			if (have_rows('menu_sub_section')) {
				while (have_rows('menu_sub_section')) : the_row();

						$GLOBALS['footer-schema'] .= '{ "@type":"MenuSection"';
						if (get_sub_field('sub_section_header')) {
							$GLOBALS['footer-schema'] .= ',"name":"' . get_sub_field('sub_section_header') . '"';





							$menu_item_count = get_sub_field('menu_items');
							$menu_item_count = count($menu_item_count);
							$current_item = 1;

							// loop through the rows Menu Items
							if (have_rows('menu_items')) {

								$GLOBALS['footer-schema'] .= ',"hasMenuItem": [';

								while (have_rows('menu_items')) : the_row();

									$GLOBALS['footer-schema'] .= '{ "@type":"MenuItem",';
										if (get_sub_field('name')) { $GLOBALS['footer-schema'] .= '"name":"' . get_sub_field('name') . '"'; }
										if (get_sub_field('description')) { $GLOBALS['footer-schema'] .= ',"description":"' . get_sub_field('description') . '"'; }
										if (get_sub_field('price')) {
											$item_price = get_sub_field('price');
											$item_price = str_replace("$,£,€", "", $item_price);
											$GLOBALS['footer-schema'] .= ',"offers": { "@type":"Offer", "price":"' . $item_price . '", "priceCurrency":"GBP" }';
										}
									$GLOBALS['footer-schema'] .= '}';
									if($current_item < $menu_item_count) { $GLOBALS['footer-schema'] .= ','; }
									++$current_item;
								endwhile;

								$GLOBALS['footer-schema'] .= ']';
							}






						}
						$GLOBALS['footer-schema'] .= '}';
						if(($current_sub_section < $menu_sub_section_count) || ($current_section < $menu_section_count)) { $GLOBALS['footer-schema'] .= ','; }
						++$current_sub_section;

				endwhile;
			};

			++$current_section;

		endwhile;

	// end menu sections
	$GLOBALS['footer-schema'] .= ']';
	if($current_section < $menu_section_count) { $GLOBALS['footer-schema'] .= ','; }
	};
	$GLOBALS['footer-schema'] .= '}';?>




<div class="s_over" <?php if ($rgba_colour) { echo 'style="background-color:' . $rgba_colour . '"';};?> >
	<div class="<?php echo $container_class ?>">

		<?php if ($intro_text) { echo '<div class="intro txt_blk">'.$intro_text.'</div>'; };

	    // check if the menu_section field has rows of data
	    if (have_rows('menu_section')) {

	    	// loop through the rows of data
			while (have_rows('menu_section')) : the_row();
			$subrows = get_sub_field_object('menu_sub_section');
			$subrowscount = (count($subrows['value']));?>

				<div class="menu_section txt_blk <?php if ($subrowscount % 2 == 0) { echo 'even'; } else { echo 'odd'; }; ?>">

					<?php
					$section_header = get_sub_field('section_header');
					$section_description = get_sub_field('main_desc');
					if ($section_header || get_sub_field('main_desc')) {
						echo '<div class="section_intro">';
						if ($section_header) {
							echo '<h2>' . $section_header . '</h2>';
						}
						if ($section_description) {
							echo '<p>' . $section_description . '</p>';
						}
						echo '</div>';
					}
					// check if the menu_items field has rows of data
				    if (have_rows('menu_sub_section')) {
						$grid_sub_item_count = 1;

						// loop through the rows of data
						while (have_rows('menu_sub_section')) : the_row();

							echo '<div class="menu_sub_section ic_' . $grid_sub_item_count . '">';
							unset($sub_section_header);
							$sub_section_header = get_sub_field('sub_section_header');
							if($sub_section_header) {
								$sub_section_ID = preg_replace("/[^A-Za-z ]/", '', $sub_section_header);
								$sub_section_ID = str_replace(' ', '', $sub_section_ID);
							}

							$price_header = get_sub_field('price_header');
							if ($sub_section_header || $price_header) {
								echo '<h3 id="' . $sub_section_ID . '"><span class="title">' . $sub_section_header . '</span><span class="price">' . $price_header . '</span></h3>';
							}
							if (get_sub_field('sub_sec_desc')) {
								echo '<p>'.get_sub_field('sub_sec_desc').'</p>';
							}


				            if (have_rows('menu_items')) {
				                while (have_rows('menu_items')) : the_row();

									echo '<h5><span class="title">' . get_sub_field('name') . '</span><span class="price">' . get_sub_field('price') . '</span><span class="symbol">' . get_sub_field('symbol') . '</span></h5>';
									if (get_sub_field('description')) {
										echo '<p>'.get_sub_field('description').'</p>';
									}


								    // check if the repeater field has rows of data
								    if (have_rows('options')) {
								        // loop through the rows of data
									    while (have_rows('options')) : the_row();
										  	echo '<p class="option">' . get_sub_field('option') . '</p>';
										endwhile;
									};
		                		endwhile;
		            		};
						echo '</div>';
						++$grid_sub_item_count;
			        	endwhile;

			        echo '</div>';
				};
	        endwhile;
		};

		$notes = get_field('notes');
	    if ($notes) { echo '<div class="notes txt_blk">'.$notes.'</div>'; };?>

		</div>
	</div>
</div>
